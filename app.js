// Task 1
const product = {
    name: "Phone",
    price: 1000.5,
    discount: 25,
}

console.log(product);


// Task 2
const greeting = (person) => {
    return `Привіт, мене звати ${person.name}, мені ${person.age} років.`;
}

let personName = prompt("Введіть своє ім'я:");
let personAge = prompt("Введіть свій вік:");


const person = {
    name: personName,
    age: personAge,
};

alert(greeting(person));


// Task 3
const copyObj = (obj) => {
    const newObj = {};
    for (const objKey in obj) {
        if (typeof obj[objKey] === 'object') {
            newObj[objKey] = copyObj(obj[objKey])
        }
        newObj[objKey] = obj[objKey];
    }
    return newObj;
};

const address = {
    city: 'city',
    street: 'street',
    house: '4',
    appartaments: 10,
};

const obj1 = {
    name: 'Name',
    age: 18,
    address: address,
};

console.log(obj1);

const obj2 = copyObj(obj1);
console.log(obj2);
